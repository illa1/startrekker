#include "RenderArea.h"
#include <QPainter>
#include "Controller.h"
#include <iostream>

RenderArea::RenderArea(QWidget* parent, Controller& conroller)
    : QWidget(parent)
    , controller_(conroller)
{
}

RenderArea::~RenderArea()
{
}

void RenderArea::paintEvent(QPaintEvent* event)
{
    std::cout << "paint event" << std::endl;
    auto size = this->size();

    controller_.setViewSize(ViewSize(size.width(), size.height()));
    for (auto&& star : controller_.getPoints())
    {
        QPen pen(Qt::white);
        pen.setStyle(Qt::DotLine);
        pen.setCapStyle(Qt::RoundCap);
        pen.setWidth(10);
        if (star.magnitude > -1) pen.setWidth(9);
        if (star.magnitude > 0) pen.setWidth(8);
        if (star.magnitude > 1) pen.setWidth(6);
        if (star.magnitude > 2) pen.setWidth(3);
        if (star.magnitude > 3.5) pen.setWidth(2);
        if (star.magnitude > 5) pen.setWidth(1);

        QPainter painter_(this);
        painter_.setPen(pen);
        painter_.drawPoint(star.x, star.y);
        if (star.magnitude < 2.5)
            painter_.drawText(QPoint(star.x+10, star.y+10), QString(star.name.c_str()));
    }
}

#pragma once

#include <QWidget>
#include <QPainter>
#include "Controller.h"

class RenderArea : public QWidget
{
Q_OBJECT
public:
    explicit RenderArea(QWidget* parent, Controller& conroller);
    ~RenderArea();

    void paintEvent(QPaintEvent* event);
private:
    Controller& controller_;
};
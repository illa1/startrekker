#pragma once

#include <QMainWindow>
#include <QImage>
#include <QLabel>
#include "RenderArea.h"
#include "Controller.h"
#include "RightDock.h"

class MainApplication : public QMainWindow
{
Q_OBJECT
public:
    explicit MainApplication(Controller& controller);
    ~MainApplication() override;
protected:
    void keyPressEvent(QKeyEvent*) override;
private:
    RenderArea renderArea_;
    RightDock rightDock_;
    Controller& controller_;
};

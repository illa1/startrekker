#pragma once

#include <iostream>
#include <string>
#include <sstream>
#include <Controller.h>
#include <QLabel>
#include <QLineEdit>
#include <QWidget>
#include <QDockWidget>
#include <QVBoxLayout>
#include <QKeyEvent>
#include <QPushButton>

namespace
{
std::string replaceComaToDot(std::string in)
{
    for (int i = 0; i < in.size(); ++i)
    {
        if (in[i] == ',') in[i] = '.';
    }
    return in;
}
}

class Content : public QWidget
{
Q_OBJECT
public:
    Content(QWidget* parent, Controller& controller)
        : QWidget(parent)
        , controller_(controller)
    {
        this->setMinimumWidth(200);
        QVBoxLayout* layout = new QVBoxLayout(this);

        positionLine_ = new QLineEdit(this);
        positionLine_->setMinimumWidth(200);
        layout->addWidget(positionLine_);

        cameraLine_ = new QLineEdit(this);
        cameraLine_->setMinimumWidth(200);
        layout->addWidget(cameraLine_);

        auto upButton = new QPushButton(this);
        upButton->setText("UP");
        auto downButton = new QPushButton(this);
        downButton->setText("Down");
        auto leftButton = new QPushButton(this);
        leftButton->setText("Left");
        auto rightButton = new QPushButton(this);
        rightButton->setText("Right");

        layout->addWidget(upButton);
        layout->addWidget(downButton);
        layout->addWidget(leftButton);
        layout->addWidget(rightButton);

        connect(leftButton, SIGNAL(clicked()), this, SLOT(onLeftClick()));
        connect(rightButton, SIGNAL(clicked()), this, SLOT(onRightClick()));
        connect(upButton, SIGNAL(clicked()), this, SLOT(onUpClick()));
        connect(downButton, SIGNAL(clicked()), this, SLOT(onDownClick()));


        this->setLayout(layout);
        connect(positionLine_, SIGNAL(editingFinished()), this, SLOT(updateLocation()));
    }

    void paintEvent(QPaintEvent* event)
    {
        if (not positionLine_->hasFocus())
        {
            auto str = toString(controller_.getLocation());
            positionLine_->setText(str.c_str());
        }
        if (not cameraLine_->hasFocus())
        {
            auto str = toString(controller_.getCameraCoordinates());
            cameraLine_->setText(str.c_str());
        }
    }
public slots:
    void updateLocation()
    {
        std::cout << "editing done" << std::endl;
        auto str = replaceComaToDot(positionLine_->text().toStdString());

        std::istringstream ss(str);
        float lattitude;
        float lognitude;

        ss >> lattitude;
        ss >> lognitude;

        geographics_coordinates::Coordinates coordinates(lattitude, lognitude);
        controller_.setNewLocation(coordinates);
    }

    void onLeftClick()
    {
        auto cords = controller_.getCameraCoordinates();
        cords.right -= 5;
        controller_.setCameraCoordinates(cords);
    }

    void onRightClick()
    {
        auto cords = controller_.getCameraCoordinates();
        cords.right += 5;
        controller_.setCameraCoordinates(cords);
    }

    void onUpClick()
    {
        auto cords = controller_.getCameraCoordinates();
        cords.declination += 5;
        controller_.setCameraCoordinates(cords);
    }

    void onDownClick()
    {
        auto cords = controller_.getCameraCoordinates();
        cords.declination -= 5;
        controller_.setCameraCoordinates(cords);
    }

private:
    QLineEdit* positionLine_;
    QLineEdit* cameraLine_;
    Controller& controller_;
};

class RightDock : public QDockWidget
{
Q_OBJECT
public:
    explicit RightDock(QWidget* parent, Controller& controller)
        : QDockWidget(parent)
    {
        this->setAllowedAreas(Qt::RightDockWidgetArea);
        auto content = new Content(this, controller);

        setWidget(content);
    }


};

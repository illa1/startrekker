#include "MainApplication.h"
#include <QHBoxLayout>
#include <QKeyEvent>
#include <iostream>

MainApplication::MainApplication(Controller& controller)
    : renderArea_(this, controller)
    , rightDock_(this, controller)
    , controller_(controller)
{
    controller_.setTriggerUpdateFunction([this](){ this->update(); });
    setStyleSheet("background-color:black;color:white;");
    setCentralWidget(&renderArea_);
    setMinimumSize(640, 480);
    addDockWidget(Qt::RightDockWidgetArea, &rightDock_);
}

MainApplication::~MainApplication()
{
}

void MainApplication::keyPressEvent(QKeyEvent* ev)
{
    std::cout << "[MainApplication] Key pressed" << ev->key() << std::endl;
    const unsigned int keyUp = 16777235;
    const unsigned int keyDown = 16777236;
    const unsigned int keyLeft = 16777234;
    const unsigned int keyRight = 16777236;
    if (ev->key() == keyUp)
    {
        auto cords = controller_.getCameraCoordinates();
        cords.declination += 1;
        controller_.setCameraCoordinates(cords);
    }
    if (ev->key() == keyDown)
    {
        auto cords = controller_.getCameraCoordinates();
        cords.declination -= 1;
        controller_.setCameraCoordinates(cords);
    }
    if (ev->key() == keyLeft)
    {
        auto cords = controller_.getCameraCoordinates();
        cords.right -= 5;
        controller_.setCameraCoordinates(cords);
    }
    if (ev->key() == keyRight)
    {
        auto cords = controller_.getCameraCoordinates();
        cords.right += 5;
        controller_.setCameraCoordinates(cords);
    }
}
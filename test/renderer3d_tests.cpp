#include <gtest/gtest.h>
#include <cmath>

#include "renderer3d.hpp"

using namespace testing;

// default camera {0,0,0}, direction {0,0,1}
// top left {-1,1,1}, top right {1,1,1}, bottom left {-1,-1,1}
TEST(Renderer3DTests, inCenter)
{
    Renderer3D r(101, 101);
    r.addPoint("star1", Point{0, 0, 10});
    auto points = r.renderPoints();
    ASSERT_EQ(points.size(), 1u);
    EXPECT_EQ(points[0].x, 50);
    EXPECT_EQ(points[0].y, 50);
}

TEST(Renderer3DTests, onBorder)
{
    Renderer3D r(101, 101);
    r.addPoint("star1", Point{-2, -1, 2});
    auto points = r.renderPoints();
    ASSERT_EQ(points.size(), 1u);
    EXPECT_EQ(points[0].x, 0);
    EXPECT_EQ(points[0].y, 25);
}

TEST(Renderer3DTests, insideScreen)
{
    Renderer3D r(101, 101);
    r.addPoint("star1", Point{-2, 1, 4});
    auto points = r.renderPoints();
    ASSERT_EQ(points.size(), 1u);
    EXPECT_EQ(points[0].x, 25);
    EXPECT_EQ(points[0].y, 63); // 100-int(3/8*100 = 37.5) = 100-37 = 63
}

TEST(Renderer3DTests, outsideScreen)
{
    Renderer3D r(101, 101);
    r.addPoint("star1", Point{0, 2.1, 2});
    auto points = r.renderPoints();
    ASSERT_EQ(points.size(), 0u);
}

TEST(Renderer3DTests, behindCamera)
{
    Renderer3D r(101, 101);
    r.addPoint("star1", Point{3, 2, -4});
    auto points = r.renderPoints();
    ASSERT_EQ(points.size(), 0u);
}

TEST(Renderer3DTests, fewPoints)
{
    Renderer3D r(101, 101);
    r.addPoint("star in center", Point{0, 0, 10});
    r.addPoint("star outside", Point{0, 2.1, 2});
    r.addPoint("star on border", Point{-2, -1, 2});
    auto points = r.renderPoints();
    ASSERT_EQ(points.size(), 2u);

    EXPECT_EQ(points[0].id, "star in center");
    EXPECT_EQ(points[0].x, 50);
    EXPECT_EQ(points[0].y, 50);

    EXPECT_EQ(points[1].id, "star on border");
    EXPECT_EQ(points[1].x, 0);
    EXPECT_EQ(points[1].y, 25);
}

TEST(Renderer3DTests, rotateCameraLeft)
{
    Renderer3D r(101, 101);
    r.addPoint("star1", Point{0, 0, 5});  // center moves to right
    r.rotateCameraRight(M_PI/4.0);
    auto points = r.renderPoints();
    ASSERT_EQ(points.size(), 1u);
    EXPECT_EQ(points[0].x, 100);
    EXPECT_EQ(points[0].y, 50);
}

TEST(Renderer3DTests, rotateCameraUp)
{
    Renderer3D r(101, 101);
    r.addPoint("star1", Point{0, 0, 5});  // center moves to down
    r.rotateCameraUp(M_PI/4.0);
    auto points = r.renderPoints();
    ASSERT_EQ(points.size(), 1u);
    EXPECT_EQ(points[0].x, 50);
    EXPECT_EQ(points[0].y, 100);
}

TEST(Renderer3DTests, moveCamera)
{
    Renderer3D r(101, 101);
    r.addPoint("star1", Point{0, 0, 2});
    r.setCameraXYZ(Point{-1, 0, -1});
    auto points = r.renderPoints();
    ASSERT_EQ(points.size(), 1u);
    EXPECT_EQ(points[0].x, 66); // 50+50/3
    EXPECT_EQ(points[0].y, 50);
}

TEST(Renderer3DTests, moveAndRotateCamera)
{
    Renderer3D r(101, 101);
    r.addPoint("star1", Point{1, -2, -2});
    r.setCameraXYZ(Point{-1, -2, -2});
    r.rotateCameraRight(-M_PI/2.0);
    r.rotateCameraUp(-M_PI/4.0);
    auto points = r.renderPoints();
    ASSERT_EQ(points.size(), 1u);
    EXPECT_EQ(points[0].x, 50);
    EXPECT_EQ(points[0].y, 0);
}

TEST(Renderer3DTests, cameraDifferentAxis)
{
    Renderer3D r(101, 101);
    r.addPoint("star", Point{5,0,5});
    r.setCameraAxis(Vector3{1,0,1}, Vector3{0,1,0});

    auto points = r.renderPoints();
    ASSERT_EQ(points.size(), 1u);
    EXPECT_EQ(points[0].x, 50);
    EXPECT_EQ(points[0].y, 50);

    r.rotateCameraRight(M_PI/4.0);
    points = r.renderPoints();
    ASSERT_EQ(points.size(), 1u);
    EXPECT_EQ(points[0].x, 0);
    EXPECT_EQ(points[0].y, 50);

    r.rotateCameraRight(-M_PI/4.0);
    r.rotateCameraUp(M_PI/4.0);
    points = r.renderPoints();
    ASSERT_EQ(points.size(), 1u);
    EXPECT_EQ(points[0].x, 50);
    EXPECT_EQ(points[0].y, 100);
}

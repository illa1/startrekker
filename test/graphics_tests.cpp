#include <gtest/gtest.h>
#include <cmath>

#include "graphics_math.hpp"

using namespace testing;
namespace {
TEST(GraphicsMathTests, MultiplyMatrix)
{
    Matrix3 m(1, 2, 3,
               4, 5, 6,
               7, 8, 9);

    Vector3 v(3, 2, 1);

    Vector3 expectedResult(10, 28, 46);
    auto result = m.multiply(v);
    EXPECT_EQ(expectedResult.x, result.x);
    EXPECT_EQ(expectedResult.y, result.y);
    EXPECT_EQ(expectedResult.z, result.z);
}

TEST(GraphicsMathTests, InverseMatrix)
{
    Matrix3 m(1, 2, 3,
              2, 1, 3,
              1, 2, 2);

    auto result = m.inverse();
    ASSERT_TRUE(result);

    Matrix3 expectedResult(-4.0/3, 2.0/3, 1.0,
                            -1.0/3, -1.0/3, 1.0,
                            1.0, 0.0 , -1.0);

    EXPECT_EQ(expectedResult.b, result->b);
    EXPECT_EQ(expectedResult.c, result->c);
    EXPECT_EQ(expectedResult.d, result->d);
    EXPECT_EQ(expectedResult.e, result->e);
    EXPECT_EQ(expectedResult.f, result->f);
    EXPECT_EQ(expectedResult.g, result->g);
    EXPECT_EQ(expectedResult.h, result->h);
    EXPECT_EQ(expectedResult.i, result->i);
}

TEST(GraphicsMathTests, InverseMatrixDoesntExist)
{
    Matrix3 m(1, 2, 3,
              2, 4, 6,
              1, 2, 2);

    EXPECT_FALSE(m.inverse());
}

TEST(GraphicsMathTests, VectorLength)
{
    Vector3 v(3,4,0);
    auto l = v.length();
    EXPECT_EQ(l, 5.0);
}

TEST(GraphicsMathTests, NormalizedVectorLength)
{
    Vector3 v(1,2,3);
    auto normalized = v.normalize();
    auto l = normalized.length();
    EXPECT_EQ(l, 1.0);
}

TEST(GraphicsMathTests, ZeroVectorLength)
{
    Vector3 v(0,0,0);
    auto normalized = v.normalize();
    EXPECT_EQ(normalized.x, 0);
    EXPECT_EQ(normalized.y, 0);
    EXPECT_EQ(normalized.z, 0);
}

TEST(GraphicsMathTests, CrossPoint)
{
    // Znaleźć punkt przecięcia prostej x=-2+3t ; y=2-t ; z=-1+2t z płaszczyzną 2x+3y+3z-8=0
    // Punkt wspólny danej prostej i płaszczyzny to punkt (1,1,1).

    Line line{Point{-2,2,-1}, Vector3{3,-1,2}};
    Plane plane{Point{1,1,1}, Vector3{3,-1,-1}, Vector3{0,1,-1}};
    auto p = findCrossPoint(line, plane);
    ASSERT_TRUE(p);

    auto cp = p->base.add(p->v1.multiply(p->coefficientV1)).add(p->v2.multiply(p->coefficientV2));
    EXPECT_EQ(cp.x, 1.0);
    EXPECT_EQ(cp.y, 1.0);
    EXPECT_EQ(cp.z, 1.0);
}

TEST(GraphicsMathTests, NoCrossPoint)
{
    Line line{Point{0,0,1}, Vector3{1,0,0}};
    Plane plane{Point{0,0,0}, Vector3{1,0,0}, Vector3{0,1,0}};
    auto p = findCrossPoint(line, plane);
    ASSERT_FALSE(p);
}

TEST(GraphicsMathTests, LineOnPlane)
{
    Line line{Point{1,2,0}, Vector3{1,1,0}};
    Plane plane{Point{0,0,0}, Vector3{1,0,0}, Vector3{0,1,0}};
    auto p = findCrossPoint(line, plane);
    ASSERT_FALSE(p);
}

bool closeTo(double a, double b)
{
    if (abs(a-b) < 0.000000000001) return true;
    std::cout << a << ", " << b << "\n";
    return false;
}

void check_close(Point p1, Point p2)
{
    EXPECT_TRUE(closeTo(p1.x, p2.x));
    EXPECT_TRUE(closeTo(p1.y, p2.y));
    EXPECT_TRUE(closeTo(p1.z, p2.z));
}

TEST(GraphicsMathTests, RotatePoint_0YAxis)
{
    double angle = M_PI/2;
    Point center {0,0,0};
    Point p1 {0,10,0};
    Point p2 {1,0,0};
    Point p3 {1,2,3};
    Point expectedResult1 = {0,10,0};
    Point expectedResult2 = {0,0,-1};
    Point expectedResult3 = {3,2,-1};

    Vector3 axis {0,1,0};

    auto r1 = rotate(p1, angle, axis, center);
    auto r2 = rotate(p2, angle, axis, center);
    auto r3 = rotate(p3, angle, axis, center);

    check_close(expectedResult1, r1);
    check_close(expectedResult2, r2);
    check_close(expectedResult3, r3);
}

TEST(GraphicsMathTests, RotatePoint_ZeroIsNotCenter)
{
    double angle = M_PI/2;
    Point center {1,1,2};
    Point p1 {1,8,2};
    Point p2 {0,0,0};
    Point p3 {2,1,3};
    Point expectedResult1 = {1,8,2};
    Point expectedResult2 = {-1,0,3};
    Point expectedResult3 = {2,1,1};

    Vector3 axis {0,1,0};

    auto r1 = rotate(p1, angle, axis, center);
    auto r2 = rotate(p2, angle, axis, center);
    auto r3 = rotate(p3, angle, axis, center);

    check_close(expectedResult1, r1);
    check_close(expectedResult2, r2);
    check_close(expectedResult3, r3);
}

TEST(GraphicsMathTests, RotatePoint_VectorAxis)
{
    double angle = M_PI/2;
    Point center {0,0,0};
    Point p1 {0,0,0};
    Point p2 {3,0,0};
    Point expectedResult1 = {0,0,0};
    Point expectedResult2 = {3.0/2,sqrt(9.0/2),3.0/2};

    Vector3 axis {1,0,1};

    auto r1 = rotate(p1, angle, axis.normalize(), center);
    auto r2 = rotate(p2, angle, axis.normalize(), center);

    check_close(expectedResult1, r1);
    check_close(expectedResult2, r2);
}

TEST(GraphicsMathTests, ProjectPointOnOx)
{
    Line l {Point{0,0,0}, Vector3{1,0,0}};
    Point p1 {10, 0, 0};
    Point expectedResult1 = {10,0,0};

    Point p2 {1, 2, 3};
    Point expectedResult2 = {1,0,0};

    auto r1 = projection(l, p1);
    auto r2 = projection(l, p2);

    EXPECT_EQ(expectedResult1.x, r1.x);
    EXPECT_EQ(expectedResult1.y, r1.y);
    EXPECT_EQ(expectedResult1.z, r1.z);

    EXPECT_EQ(expectedResult2.x, r2.x);
    EXPECT_EQ(expectedResult2.y, r2.y);
    EXPECT_EQ(expectedResult2.z, r2.z);
}

TEST(GraphicsMathTests, ProjectPointOnLine)
{
    Line l {Point{1,0,0}, Vector3{1,0,3}};
    Point p1 {8, 0, 1};
    Point expectedResult1 = {2,0,3};

    Point p2 {3, 10, 6};
    Point expectedResult2 = {3,0,6};

    auto r1 = projection(l, p1);
    auto r2 = projection(l, p2);

    EXPECT_EQ(expectedResult1.x, r1.x);
    EXPECT_EQ(expectedResult1.y, r1.y);
    EXPECT_EQ(expectedResult1.z, r1.z);

    EXPECT_EQ(expectedResult2.x, r2.x);
    EXPECT_EQ(expectedResult2.y, r2.y);
    EXPECT_EQ(expectedResult2.z, r2.z);
}

TEST(GraphicsMathTests, OrthogonalVector)
{
    Vector3 v1 = {1,0,0};
    Vector3 v2 = {0,1,0};
    auto result1 = orthogonalVector(v1, v2);

    Point expectedResult1 = {0,0,1};
    EXPECT_EQ(expectedResult1.x, result1.x);
    EXPECT_EQ(expectedResult1.y, result1.y);
    EXPECT_EQ(expectedResult1.z, result1.z);

    auto result2 = orthogonalVector(v1, v1);
    EXPECT_EQ(0, result2.x);
    EXPECT_EQ(0, result2.y);
    EXPECT_EQ(0, result2.z);
}

}

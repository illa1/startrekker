#include <gtest/gtest.h>

#include "earth_geometry.hpp"

using namespace testing;

namespace {

bool closeTo(double a, double b)
{
    if (abs(a-b) < 0.0000000001) return true;
    std::cout << a << ", " << b << "\n";
    return false;
}

TEST(EarthGeometryTests, CalculateStarPosition)
{
    StarPosition star1 = {0, 0, 10.0};
    auto p1 = EarthGeometry::starPosition(star1);
    EXPECT_TRUE(closeTo(p1.x, 10.0));
    EXPECT_TRUE(closeTo(p1.y, 0.0));
    EXPECT_TRUE(closeTo(p1.z, 0.0));

    StarPosition star2 = {-EarthGeometry::EARTH_TILT, M_PI/2, 10.0};
    auto p2 = EarthGeometry::starPosition(star2);
    EXPECT_TRUE(closeTo(p2.x, 0.0));
    EXPECT_TRUE(closeTo(p2.y, 0.0));
    EXPECT_TRUE(closeTo(p2.z, -10.0));
}

TEST(EarthGeometryTests, HalfADay)
{
    // TO DO
}

TEST(EarthGeometryTests, PositionOfObserver)
{
    // TO DO
}

}

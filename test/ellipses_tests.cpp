#include <gtest/gtest.h>
#include <cmath>

#include "ellipses.hpp"

using namespace testing;

namespace {
bool closeTo(double a, double b)
{
    if (abs(a-b) < 0.0000000001) return true;
    std::cout << a << ", " << b << "\n";
    return false;
}

TEST(Ellipses2DTests, Circle2D)
{
    Ellipse2D e = {2, 2};

    auto focus = e.getFocus();
    EXPECT_EQ(focus.x, 0);
    EXPECT_EQ(focus.y, 0);

    auto right = e.getPoint(0);
    EXPECT_TRUE(closeTo(right.x, 2.0));
    EXPECT_TRUE(closeTo(right.y, 0.0));

    auto top = e.getPoint(M_PI/2);
    EXPECT_TRUE(closeTo(top.x, 0.0));
    EXPECT_TRUE(closeTo(top.y, 2.0));

    auto inMiddle = e.getPoint(M_PI/4);
    EXPECT_TRUE(closeTo(inMiddle.x, sqrt(2.0)));
    EXPECT_TRUE(closeTo(inMiddle.y, sqrt(2.0)));
}

TEST(Ellipses2DTests, EllipsePoints)
{
    Ellipse2D e = {3, 2};

    auto right = e.getPoint(0);
    EXPECT_TRUE(closeTo(right.x, 3.0));
    EXPECT_TRUE(closeTo(right.y, 0.0));

    auto top = e.getPoint(M_PI/2);
    EXPECT_TRUE(closeTo(top.x, 0.0));
    EXPECT_TRUE(closeTo(top.y, 2.0));
}

TEST(Ellipses2DTests, AreaOfEllipse)
{
    // pole Pi*a*b
    Ellipse2D e = {3, 2};

    auto delta = abs(e.approximateArea() - 6*M_PI);
    EXPECT_TRUE(delta < 0.0001); // not really close?
}
}

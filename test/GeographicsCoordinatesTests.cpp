#include "GeographicsCoordinates.h"
#include <gtest/gtest.h>

bool almostEqual(float lhs, float rhs)
{
    return abs(lhs - rhs) <= abs(lhs*0.0000001);
}

TEST(GeographicsCoordinates, shallProperlyRecalculateDMSToDecimal)
{
    auto in = geographics_coordinates::DMS{51,06,16.2};
    ASSERT_TRUE(almostEqual(geographics_coordinates::DMSToDecimal(in), 51.104504));
}

TEST(GeographicsCoordinates, shallProperlyRecalculateDecimalToDMS)
{
    auto in = 51.104504f;
    auto out = geographics_coordinates::DMSToDecimal(in);
    ASSERT_EQ(out.degrees_, 51);

}
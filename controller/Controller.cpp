#include "Controller.h"
#include "sky.hpp"
#include "renderer3d.hpp"

#include <numbers>

namespace
{
// TODO: Obviously we need to have something like Units modue
double degreesToRadians(int degrees)
{
    return 2*M_PI * degrees/360;
}
}

std::string toString(geographics_coordinates::Coordinates in)
{
    std::string out;
    out += std::to_string(in.latitude_) + " " + std::to_string(in.longitude_);
    return out;
}

std::string toString(UserLookAtDirection in)
{
    std::string out;
    out += std::to_string(in.right) + " " + std::to_string(in.declination);
    return out;
}

Controller::Controller()
{
    sky_ = new Sky();
}

Controller::~Controller()
{
    delete sky_;
}

void Controller::init()
{
}

void Controller::setViewSize(ViewSize viewSize)
{
    viewSize_ = viewSize;
    // triggerUpdate_();
}

void Controller::setNewLocation(geographics_coordinates::Coordinates userLocation)
{
    userLocation_ = userLocation;
    triggerUpdate_();
}

std::vector<StarData> Controller::getPoints()
{
    double time = 0;
    Renderer3D renderer(viewSize_.width_, viewSize_.height_);

    auto cameraSettings = sky_->calculateObserverPosition(time,
        Coordinates{degreesToRadians(userLocation_.latitude_), degreesToRadians(userLocation_.longitude_)});

    renderer.setCameraXYZ(cameraSettings.position);
    renderer.setCameraAxis(cameraSettings.toNorth, cameraSettings.othogonalToEarth);

    auto skyObjects = sky_->calculateObjectPositions(time);
    for (auto object : skyObjects) renderer.addPoint(object.id, object.position);

    renderer.rotateCameraRight(degreesToRadians(userLookAtDirection_.right));
    renderer.rotateCameraUp(degreesToRadians(userLookAtDirection_.declination));
    auto points = renderer.renderPoints();

    auto findMagnitude = [&skyObjects](auto name)
    {
        for (auto & el : skyObjects) if (el.id == name) return el.magnitude;
        return 10.0;
    };
    std::vector<StarData> out;
    for (auto&& star: points)
    {
        StarData data;
        data.name = star.id;
        data.x = star.x;
        data.y = star.y;
        data.magnitude = findMagnitude(data.name);
        out.push_back(data);
    }

    return out;
}

geographics_coordinates::Coordinates Controller::getLocation()
{
    return this->userLocation_;
}

UserLookAtDirection Controller::getCameraCoordinates()
{
    return this->userLookAtDirection_;
}

void Controller::setCameraCoordinates(UserLookAtDirection userLookAtDirection)
{
    userLookAtDirection_ = userLookAtDirection;
    triggerUpdate_();
}

void Controller::setTriggerUpdateFunction(std::function<void()> triggerUpdate)
{
    triggerUpdate_ = triggerUpdate;
}



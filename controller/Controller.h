#pragma once

#include <functional>
#include <string>
#include "GeographicsCoordinates.h"

using RADIAN = float;

struct StarData
{
    int x;
    int y;
    float magnitude;
    std::string name;
};

struct ViewSize
{
    int width_;
    int height_;
};
std::string toString(geographics_coordinates::Coordinates in);

struct UserLookAtDirection
{
    RADIAN right = 0.0f;
    RADIAN declination = 0.0f;
};
std::string toString(UserLookAtDirection in);

class Sky;

class Controller
{
public:
    Controller();
    ~Controller();
	
    void setTriggerUpdateFunction(std::function<void()> triggerUpdate);

    void init();
    void setViewSize(ViewSize viewSize);
    void setNewLocation(geographics_coordinates::Coordinates userLocation);
    geographics_coordinates::Coordinates getLocation();
    void setCameraCoordinates(UserLookAtDirection userLookAtDirection);
    UserLookAtDirection getCameraCoordinates();
    std::vector<StarData> getPoints();

private:
    ViewSize viewSize_;
    geographics_coordinates::Coordinates userLocation_ = {51.104504, 17.005084};
    UserLookAtDirection userLookAtDirection_;

    std::function<void()> triggerUpdate_;
    Sky* sky_;
};

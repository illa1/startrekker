#ifndef UNITS_H
#define UNITS_H

constexpr long KM = 1;
constexpr long long MKM = 1000000;
constexpr long long LY = MKM *  (946 * 10000);
constexpr long DAY = 24*60;  // minutes
constexpr double EARTH_RADIUS = 6371 * KM;

#endif // UNITS_H


#ifndef __SKY__
#define __SKY__

#include <vector>
#include "earth_geometry.hpp"
#include "planet.hpp"


struct Star
{
    std::string id;
    StarPosition starPosition;
    double magnitude;
};

struct PointData
{
    std::string id;
    Point position;
    double magnitude;
};

struct ObserverPosition
{
    Point position;
    Vector3 othogonalToEarth;
    Vector3 toNorth;
};

class Sky
{
public:
    Sky();
    std::vector<PointData> calculateObjectPositions(double now);
    ObserverPosition calculateObserverPosition(double now, Coordinates coordinates);
private:
    double makeGlobalTime(int year, int month, int day, int hour, int min);
    double calculateInitAngle(double declination, double rightAscension, OrbitAngle orbitAngle);
    void initPlanets();
    void calculateStarsPositions();
    void initStars();
    double hoursToRadians(int h, int m, double s) const;
    double degreesToRadians(int degrees, int m, double s) const;
    double degreesToRadians(double degrees) const;

    Planet earth{"",0,0,0,{0,0},{0,0,0}};
    std::vector<Planet> planets;
    std::vector<Star> stars;
    std::vector<PointData> starPositions;
};
#endif

#include "renderer3d.hpp"

#include <cmath>

Renderer3D::Renderer3D(int width, int height)
{
    this->width = width - 1;
    this->height = height - 1;
}

void Renderer3D::addPoint(const std::string & id, const Point & p)
{
    points_.push_back(std::make_pair(p, id));
}

std::vector<PixelData> Renderer3D::renderPoints()
{
    std::vector<PixelData> pixels;
    for (const auto & pointInfo : points_)
    {
        auto & p = pointInfo.first;
        auto cp = findCrossPoint(
            Line{view.camera, p.substract(view.camera)},
            Plane{view.topLeft, view.topRight.substract(view.topLeft), view.bottomLeft.substract(view.topLeft)});
        if (!cp) continue;
        auto xy = toPixelPosition(pointInfo.second, *cp);
        if (xy)
        {
            pixels.push_back(*xy);
        }
    }
    return pixels;
}

void Renderer3D::rotateCameraRight(double angle)
{
    auto rotateAxis = view.axisY;
    auto center = view.camera;

    view.topLeft = rotate(view.topLeft, -angle, rotateAxis, center);
    view.topRight = rotate(view.topRight, -angle, rotateAxis, center);
    view.bottomLeft = rotate(view.bottomLeft, -angle, rotateAxis, center);
}

void Renderer3D::rotateCameraUp(double angle)
{
    auto rotateAxis = view.topLeft.substract(view.topRight).normalize();
    auto center = view.camera;

    view.topLeft = rotate(view.topLeft, -angle, rotateAxis, center);
    view.topRight = rotate(view.topRight, -angle, rotateAxis, center);
    view.bottomLeft = rotate(view.bottomLeft, -angle, rotateAxis, center);
}

void Renderer3D::setCameraXYZ(const Point & p)
{
    auto delta = p.substract(view.camera);
    view.camera = p;
    view.topLeft = view.topLeft.add(delta);
    view.topRight = view.topRight.add(delta);
    view.bottomLeft = view.bottomLeft.add(delta);
}

void Renderer3D::setCameraAxis(Vector3 lookDirection, Vector3 axisY)
{
    lookDirection = lookDirection.normalize();
    view.axisY = axisY.normalize();
    auto orthogonalV = orthogonalVector(lookDirection, axisY).normalize();

    //lookDirection = lookDirection.multiply(1.5);
    view.topLeft = view.camera.add(lookDirection).substract(axisY).substract(orthogonalV);  //myabe add(axisY) ?
    view.topRight = view.camera.add(lookDirection).substract(axisY).add(orthogonalV);
    view.bottomLeft = view.camera.add(lookDirection).add(axisY).substract(orthogonalV);
}

auto getVisibleMin(double ratio)
{
    return 0.5 - (0.5 / ratio);
}

std::optional<PixelData> Renderer3D::toPixelPosition(const std::string & id, const PointV & p) const
{
    if (auto isNotInVisibleHemisphere = p.coefficientV3 < 0) return {};

    double x = p.coefficientV1;
    double y = p.coefficientV2;

    if (auto isPanoramic = width >= height)
    {
        double ratio = static_cast<double>(width) / height;
        y = (y - getVisibleMin(ratio)) * ratio;
    }
    else
    {
        double ratio = static_cast<double>(height) / width;
        x = (x - getVisibleMin(ratio)) * ratio;
    }

    if (x < 0.0 || x > 1.0) return {};
    if (y < 0.0 || y > 1.0) return {};
    return PixelData{id, int(x * width), height-int(y * height)};
}


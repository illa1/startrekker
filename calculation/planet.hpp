#ifndef __PLANET__
#define __PLANET__

#include "ellipses.hpp"

#include <string>

struct OrbitAngle
{
    double inclination;
    double longtitudeOfAscendingNode;
    double argumentOfPeriapsis;
};

class Planet
{
public:
    Planet(const std::string & id, double year,
           double initTime, double initAngle,
           const Ellipse2D & ellipse, OrbitAngle orbitAngle);
    Point findEndPosition(double time) const;
    std::string id;

private:
    Point tranformTo3DPoint(Point2D p) const;
    Point toRealOrbit(Point p) const;
    double calculateDeltaTime(double currentTime) const;

    double year;
    double initTime;
    double initAngle;

    Ellipse2D ellipse;
    OrbitAngle orbitAngle;
};

#endif

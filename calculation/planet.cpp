#include "planet.hpp"

Planet::Planet(const std::string & id,
    double year,
    double initTime,
    double initAngle,
    const Ellipse2D & ellipse,
    OrbitAngle orbitAngle)
    : id(id)
    , year(year)
    , initTime(initTime)
    , initAngle(initAngle)
    , ellipse(ellipse)
    , orbitAngle(orbitAngle)
{}

Point Planet::findEndPosition(double time) const
{
    double deltaTime = calculateDeltaTime(time);
    while (deltaTime >= year) deltaTime -= year;

    auto percent = deltaTime/year;
    auto expectedArea = percent*ellipse.approximateArea();

    auto angle = ellipse.findArcOfArea(initAngle, expectedArea);

    auto p = ellipse.getPoint(initAngle+angle);
    return toRealOrbit(tranformTo3DPoint(p));
}

Point Planet::tranformTo3DPoint(Point2D p) const
{
    return {p.x, 0, p.y};
}

Point Planet::toRealOrbit(Point p) const
{
    Point center = tranformTo3DPoint(ellipse.getFocus());

    Point zero = {0,0,0};
    Vector3 yAxis(0,1,0);
    Point semiMinorAxis = tranformTo3DPoint(ellipse.getSemiMinorAxisVector());
    Point orthogonal(0,1,0);

    // ascending node, around Y axis
    p = rotate(p, orbitAngle.longtitudeOfAscendingNode, yAxis, center);
    semiMinorAxis = rotate(semiMinorAxis, orbitAngle.longtitudeOfAscendingNode, yAxis, center);
    zero = rotate(zero, orbitAngle.longtitudeOfAscendingNode, yAxis, center);

    // inclination, around semi minor axis
    p = rotate(p, orbitAngle.inclination, semiMinorAxis.substract(zero), center);
    orthogonal = rotate(orthogonal, orbitAngle.inclination, semiMinorAxis.substract(zero), center);
    zero = rotate(zero, orbitAngle.inclination, semiMinorAxis.substract(zero), center);

    // argument of periapsis, around axis orthogonal to ellipse
    p = rotate(p, orbitAngle.argumentOfPeriapsis, orthogonal.substract(zero), center);
    return p;
}

double Planet::calculateDeltaTime(double currentTime) const
{
    return currentTime-initTime;  // FIX IT!!
}

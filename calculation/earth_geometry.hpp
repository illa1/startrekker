#ifndef __EARTH_GEOMETRY__
#define __EARTH_GEOMETRY__

#include "graphics_math.hpp"
#include <cmath>
#include <numbers>

struct Coordinates
{
    double latitude;
    double longtitude;
};

struct StarPosition
{
    double declination;
    double rightAscension;
    double distance;
};

class EarthGeometry
{
public:

    static Vector3 earthRadiusVector(Coordinates observerPosition, double time);
    static Point starPosition(const StarPosition & star);

    static constexpr double EARTH_TILT = 23.4 * 2 * std::numbers::pi / 360;
};
#endif

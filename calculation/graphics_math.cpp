#include "graphics_math.hpp"

#include <cmath>

Vector3::Vector3(double x, double y, double z)
{
    this->x = x;
    this->y = y;
    this->z = z;
}

Vector3 Vector3::add(const Vector3 & v) const
{
    return Vector3(x + v.x,
                   y + v.y,
                   z + v.z);
}

Vector3 Vector3::substract(const Vector3 & v) const
{
    return Vector3(x - v.x,
                   y - v.y,
                   z - v.z);
}

Vector3 Vector3::multiply(double a) const
{
    return Vector3(a*x,
                   a*y,
                   a*z);
}

double Vector3::length() const
{
    return sqrt(x*x + y*y + z*z);
}

Vector3 Vector3::normalize() const
{
    auto vLength = length();
    if (vLength == 0) return Vector3(0,0,0);
    return Vector3(x/vLength, y/vLength, z/vLength);
}

Matrix3::Matrix3(double a, double b, double c,
        double d, double e, double f,
        double g, double h, double i)
{
    this->a = a;
    this->b = b;
    this->c = c;
    this->d = d;
    this->e = e;
    this->f = f;
    this->g = g;
    this->h = h;
    this->i = i;
}

std::optional<Matrix3> Matrix3::inverse() const
{
    double detM = det();
    if (detM == 0) return {};

    Matrix3 adj = Matrix3(
        det2(e,f,h,i), det2(d,f,g,i), det2(d,e,g,h),
        det2(b,c,h,i), det2(a,c,g,i), det2(a,b,g,h),
        det2(b,c,e,f), det2(a,c,d,f), det2(a,b,d,e)
    );

    return Matrix3(
         adj.a/detM, -adj.d/detM,  adj.g/detM,
        -adj.b/detM,  adj.e/detM, -adj.h/detM,
         adj.c/detM, -adj.f/detM,  adj.i/detM
    );
}

Vector3 Matrix3::multiply(const Vector3 & v) const
{
    return Vector3(a*v.x + b*v.y + c*v.z,
                   d*v.x + e*v.y + f*v.z,
                   g*v.x + h*v.y + i*v.z);
}

double Matrix3::det2(double a, double b, double c, double d) const
{
    /*
     * a b
     * c d
     * */
    return a*d - b*c;
}

double Matrix3::det() const
{
    return a*e*i + b*f*g + c*d*h - a*f*h - b*d*i - c*e*g;
}

std::optional<PointV> findCrossPoint(const Line & l, const Plane & p)
{
    // p.v1*a + p.v2*b - l.v*c = l.p - p.p
    Matrix3 m(
        p.v1.x, p.v2.x, -l.v.x,
        p.v1.y, p.v2.y, -l.v.y,
        p.v1.z, p.v2.z, -l.v.z);

    auto inversed = m.inverse();
    if (!inversed) return {};

    // abcV = {a, b, c}
    // point is l.p + l.v.multiply(abcV.z);
    // or
    // point is p.p + p.v1.multiply(abcV.x) + p.v2.multiply(abcV.y);
    auto v = l.p.substract(p.p);
    auto abcV = inversed->multiply(v);
    return PointV{p.p, p.v1, p.v2, l.v, abcV.x, abcV.y, abcV.z};
}

// rotate axis must be normalized
Point rotate(Point p, double angle, Vector3 rotateAxis, Point center)
{
    auto a = rotateAxis.x;
    auto b = rotateAxis.y;
    auto c = rotateAxis.z;

    auto sinA = sin(angle);
    auto cosA = cos(angle);

    Matrix3 r(a*a*(1-cosA) +   cosA, a*b*(1-cosA) - c*sinA, a*c*(1-cosA) + b*sinA,
              a*b*(1-cosA) + c*sinA, b*b*(1-cosA) +   cosA, b*c*(1-cosA) - a*sinA,
              a*c*(1-cosA) - b*sinA, b*c*(1-cosA) + a*sinA, c*c*(1-cosA) +   cosA);

    return r.multiply(p.substract(center)).add(center);
}

Point projection(Line l, Point b)
{
    // a + kv = p
    // v o (b-p) = 0  iloczyn skalarny
    auto a = l.p;
    auto v = l.v;

    auto k = (v.x*(b.x-a.x) + v.y*(b.y-a.y) + v.z*(b.z-a.z)) / (v.x*v.x + v.y*v.y + v.z*v.z);
    return a.add(v.multiply(k));
}

Vector3 orthogonalVector(const Vector3 & a, const Vector3 & b)
{
   auto x = a.y*b.z - a.z*b.y;
   auto y = a.z*b.x - a.x*b.z;
   auto z = a.x*b.y - a.y*b.x;
   return Vector3{x, y, z};
}

// ortjogonal vector should be normalized
Point projection(Point onPlane, Vector3 orthogonal, Point p)
{
    double a = orthogonal.x;
    double b = orthogonal.y;
    double c = orthogonal.z;
    auto v = onPlane.substract(p);
    // p + k*orthogonal = result
    double k = (a*v.x + b*v.y + c*v.z)/(a*a + b*b + c*c);
    return p.add(orthogonal.multiply(k));
}

#ifndef __RENDERER3D__
#define __RENDERER3D__

#include "graphics_math.hpp"
#include <vector>
#include <string>
#include <utility>

struct PixelData
{
    std::string id;
    int x;
    int y;
};

struct View
{
    Point camera = Point(0,0,0);

    Point topLeft = Point(-1,1,1);
    Point topRight = Point(1,1,1);
    Point bottomLeft = Point(-1,-1,1);

    Vector3 axisY = Point(0,1,0);
};

class Renderer3D
{
public:
    Renderer3D(int width, int height);

    void addPoint(const std::string & id, const Point & p);
    std::vector<PixelData> renderPoints();
    void rotateCameraRight(double angle);
    void rotateCameraUp(double angle);
    void setCameraXYZ(const Point & p);
    void setCameraAxis(Vector3 lookDirection, Vector3 axisY);

private:

    std::optional<PixelData> toPixelPosition(const std::string & id, const PointV & p) const;

    int width;
    int height;

    View view;

    std::vector<std::pair<Point, std::string>> points_;
};

#endif

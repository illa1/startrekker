#pragma once

namespace geographics_coordinates
{

enum class Orientation
{
    north,
    south,
    east,
    west,
};

struct DMS
{
    int degrees_;
    int minutes_;
    float seconds_;
};

float DMSToDecimal(DMS in);
DMS DMSToDecimal(float in);

struct Coordinates
{
    Coordinates();
    Coordinates(float latitude, float longitude);
    Coordinates(float latitude, Orientation latitudeOrientation, float longitude, Orientation longitudeOrientation);
    Coordinates(DMS latitude, DMS longitude);
    Coordinates(DMS latitude, Orientation latitudeOrientation, DMS longitude, Orientation longitudeOrientation);

    float latitude_ = 0.0;
    Orientation latitudeOrientation_ = Orientation::north;
    float longitude_ = 0.0;
    Orientation longitudeOrientation_ = Orientation::east;
};


};


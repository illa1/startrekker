#ifndef __GRAPHICS_MATH__
#define __GRAPHICS_MATH__

#include <optional>

struct Vector3
{
    Vector3(double x, double y, double z);

    Vector3 add(const Vector3 & v) const;
    Vector3 substract(const Vector3 & v) const;
    Vector3 multiply(double a) const;
    Vector3 normalize() const;
    double length() const;

    double x;
    double y;
    double z;
};

class Matrix3
{
    /*
     * a b c
     * d e f
     * g h i
     * */
public:

    Matrix3(double a, double b, double c,
            double d, double e, double f,
            double g, double h, double i);

    std::optional<Matrix3> inverse() const;
    Vector3 multiply(const Vector3 & v) const;

    double a;
    double b;
    double c;

    double d;
    double e;
    double f;

    double g;
    double h;
    double i;

private:
    double det2(double a, double b, double c, double d) const;
    double det() const;
};

typedef Vector3 Point;

struct Line
{
    Point p;
    Vector3 v;
};

struct Plane
{
    Point p;
    Vector3 v1;
    Vector3 v2;
};

struct PointV
{
    Point base;

    Vector3 v1;
    Vector3 v2;
    Vector3 v3;

    double coefficientV1;
    double coefficientV2;
    double coefficientV3;
};

std::optional<PointV> findCrossPoint(const Line & l, const Plane & p);
Point rotate(Point p, double angle, Vector3 rotateAxis, Point center);
Point projection(Line l, Point b);
Vector3 orthogonalVector(const Vector3 & a, const Vector3 & b);
Point projection(Point onPlane, Vector3 orthogonal, Point p);
#endif

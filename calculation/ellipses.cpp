#include "ellipses.hpp"

#include <cmath>
#include <numbers>

// e*e = (a*a - b*b)/a*a
double calculateSemiMinorAxis(double semiMajorAxis, double eccentricity)
{
    if (eccentricity == 0.0) return semiMajorAxis;
    return sqrt(semiMajorAxis*semiMajorAxis*(1.0-eccentricity*eccentricity));
}

Ellipse2D::Ellipse2D(double semiMajorAxis, double semiMinorAxis)
: semiMajorAxis(semiMajorAxis)
, semiMinorAxis(semiMinorAxis)
{
    focus = sqrt(semiMajorAxis*semiMajorAxis-semiMinorAxis*semiMinorAxis);
}

Point2D Ellipse2D::getFocus() const
{
    return {focus, 0};
}

Vector2 Ellipse2D::getSemiMinorAxisVector() const
{
    return {0, 1};
}

// sum of many small triangles
// possible change: closer to Focus -> bigger steps ?
double Ellipse2D::approximateArea() const
{
    double area = 0.0;
    double angle = 0.0;
    double step = 0.00001;
    auto p1 = getPoint(angle);
    while (angle < std::numbers::pi)
    {
        angle += step;
        auto p2 = getPoint(angle);
        area += arcArea(p1, p2);
        p1 = p2;
    }
    return 2*area;
}

double Ellipse2D::findArcOfArea(double initAngle, double expectedArea) const
{
    double area = 0.0;
    double angle = initAngle;
    double step = 0.00001;
    auto p1 = getPoint(angle);
    while (area < expectedArea)
    {
        angle += step;
        auto p2 = getPoint(angle);
        area += arcArea(p1, p2);
        p1 = p2;
    }
    return angle-initAngle;
}

Point2D Ellipse2D::getPoint(double angle) const
{
    auto a = semiMajorAxis;
    auto b = semiMinorAxis;
    auto x = a*cos(angle);
    auto y = b*sin(angle);
    return Point2D{x, y};
}

double Ellipse2D::arcArea(Point2D p1, Point2D p2) const
{
    Point p1_3d = {p1.x, p1.y, 0};
    Point p2_3d = {p2.x, p2.y, 0};
    Point focus_3d = {focus, 0, 0};

    auto p3_3d = projection(Line{p1_3d, p2_3d.substract(p1_3d)}, focus_3d);
    auto side = p1_3d.substract(p2_3d).length();
    auto h = focus_3d.substract(p3_3d).length();

    return side*h/2;
}

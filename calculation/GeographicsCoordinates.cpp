#include "GeographicsCoordinates.h"

namespace geographics_coordinates
{

float DMSToDecimal(DMS in)
{
    return in.degrees_ + in.minutes_/60.0 + in.seconds_ / (60.0*60.0);
}

DMS DMSToDecimal(float in)
{
    DMS out;
    out.degrees_ = in / 1;
    in -= out.degrees_;
    out.minutes_ = in * 60;
    in -= out.minutes_ / 60;
    out.minutes_ = in * 60 * 60;

    return out;
}

Coordinates::Coordinates()
{
}

Coordinates::Coordinates(float latitude, Orientation latitudeOrientation, float longitude, Orientation longitudeOrientation)
    : latitude_(latitude)
    , latitudeOrientation_(latitudeOrientation)
    , longitude_(longitude)
    , longitudeOrientation_(longitudeOrientation)
{
}

Coordinates::Coordinates(float latitude, float longitude)
    : Coordinates(latitude, Orientation::north, longitude, Orientation::east)  //TODO
{
}

}
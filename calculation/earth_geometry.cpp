#include "earth_geometry.hpp"

namespace
{

const double INIT_ORIENTATION = 0;  // ?
const double STELLAR_DAY = 23.90;  // ?

double spinEarth(double time)
{
    while (time > STELLAR_DAY) time -= STELLAR_DAY;
    auto percent = time/STELLAR_DAY;

    return percent * 2 * std::numbers::pi;
}

}

Vector3 EarthGeometry::earthRadiusVector(Coordinates observerPosition, double time)
{
    Point radiusVector = {1,0,0};

    Vector3 spinAxis = {0,1,0};
    Vector3 equator90 = {0,0,1};
    Point center = {0,0,0};

    Vector3 toAries = {1,0,0}; // do punktu barana
    spinAxis = rotate(spinAxis, EARTH_TILT, toAries, center);
    equator90 = rotate(equator90, EARTH_TILT, toAries, center);

    // longtitude
    double dayAngle = spinEarth(time) + INIT_ORIENTATION;
    radiusVector = rotate(radiusVector, observerPosition.longtitude + dayAngle, spinAxis, center);
    equator90 = rotate(equator90, observerPosition.longtitude + dayAngle, spinAxis, center);

    // latitude
    radiusVector = rotate(radiusVector, observerPosition.latitude, equator90, center);

    return radiusVector;
}

Point EarthGeometry::starPosition(const StarPosition & star)
{
    Vector3 spinAxis = {0,1,0};
    Vector3 equator90 = {0,0,1};
    Vector3 p = {1,0,0};

    Point center = {0,0,0};

    Vector3 toAries = {1,0,0}; // do punktu barana
    spinAxis = rotate(spinAxis, EARTH_TILT, toAries, center);
    equator90 = rotate(equator90, EARTH_TILT, toAries, center);

    // rightAscension = polozenie xz
    p = rotate(p, star.rightAscension, spinAxis, center);
    equator90 = rotate(equator90, star.rightAscension, spinAxis, center);

    // declination = wysokosc y
    p = rotate(p, star.declination, equator90, center);
    return p.multiply(star.distance);
}

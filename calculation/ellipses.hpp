#ifndef __ELLIPSES__
#define __ELLIPSES__

#include "graphics_math.hpp"

double calculateSemiMinorAxis(double semiMajorAxis, double eccentricity);

struct Point2D
{
   double x;
   double y;
};

typedef Point2D Vector2;

struct Ellipse2D
{
    Ellipse2D(double semiMajorAxis, double semiMinorAxis);


    Point2D getFocus() const;
    Vector2 getSemiMinorAxisVector() const;
    double approximateArea() const;
    double findArcOfArea(double initAngle, double expectedArea) const;

    Point2D getPoint(double angle) const;

private:
    double arcArea(Point2D p1, Point2D p2) const;

    double semiMajorAxis;
    double semiMinorAxis;
    double focus;
};

#endif

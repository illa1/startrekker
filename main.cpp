#include <QApplication>
#include "MainApplication.h"
#include "Controller.h"

int main(int argc, char** argv)
{
    Controller controller;
    controller.init();
    QApplication app(argc, argv);
    MainApplication window(controller);
    window.show();
    return app.exec();
}
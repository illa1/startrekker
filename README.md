# StarTrekker

## Project preparation.
Project assumes you have installed CMake, qt6-base, gtest. In Ubuntu 22.04 for Qt it is enought to write:
```
sudo apt install qt6-base-dev
sudo apt install qt6-base-dev-tools
sudo apt install libgl1-mesa-dev
```
GTest library needs to be compiled and copied to /usr/lib directory:
```
sudo apt install libgtest-dev
cd /usr/src/gtest
sudo cmake .
sudo make
sudo cp lib/libtest.a lib/libgtest_main.a /usr/lib
```

## Description

## Authors and acknowledgment

## License
TO DO
